Just a small [website about me](https://spri.nz) served by [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) and built using the [Hugo-Coder](https://themes.gohugo.io/hugo-coder/) theme (with heavy customizations ^^).

## Development

```bash
./lint.sh       # to enforce code style using prettier
hugo server -D  # to start a dev server
```
