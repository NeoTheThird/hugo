+++
authors = ["Jan Sprinz"]
date = "2019-10-12"
title = "Meet the UBports Installer"
subtitle = "The easy way of installing Ubuntu Touch on your device!"
conference = "Ubucon Europe"
city = "Sintra"
description = "Installing custom operating systems on Android devices is often cumbersome and difficult. But does it have to be? We will take a look at the user-friendly UBports Installer (a tool that allows even inexperienced users to install Ubuntu Touch on their devices) and explore how it is built."
youtube = "2ZCDOe1CjZ8"
[[links]]
  label = "ODP"
  link = "https://github.com/NeoTheThird/talks/blob/master/ubucon-2019-installer-english.odp"
[[links]]
  label = "Schedule"
  link = "https://manage.ubucon.org/eu2019/talk/MZSE8V/"
+++
