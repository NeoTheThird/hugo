+++
authors = ["Jan Sprinz"]
date = "2019-12-28"
title = "Ubuntu Touch & Co"
subtitle = "GNU/Linux in der Hosentasche"
conference = "Open Infrastructure Orbit at 36th Chaos Communication Congress (36c3)"
city = "Leipzig"
description = "Der Markt für Mobilgeräte wird dominiert von Android und der Anteil freier Komponenten wird merklich kleiner. Einige Projekte versuchen das zu ändern und bringen GNU/Linux auf Handies und Tablets zu bringen. Wir schauen uns verschiedene solche Projekte an und sprechen über die Notwendigkeit, den aktuellen Stand und die Zukunftsaussichten von GNU/Linux auf Mobilgeräten. Am Beispiel von Ubuntu Touch gehen wir auf besondere Herausforderungen ein."
mp4 = "https://cdn.media.ccc.de/congress/2019/h264-hd/36c3-oio-171-deu-Ubuntu_Touch_Co_-_GNU_Linux_in_der_Hosentasche_hd.mp4#t=3"
mp3 = "https://cdn.media.ccc.de/congress/2019/mp3/36c3-oio-171-deu-Ubuntu_Touch_Co_-_GNU_Linux_in_der_Hosentasche_mp3.mp3"
ogg = "https://cdn.media.ccc.de/congress/2019/opus/36c3-oio-171-deu-Ubuntu_Touch_Co_-_GNU_Linux_in_der_Hosentasche_opus.opus"
youtube = "0qdiSCx_N_8"
featured_image = "/images/36c3-oio.jpeg"
featured = true
[[links]]
  label = "ODP"
  link = "https://github.com/NeoTheThird/talks/blob/master/36c3-german.odp"
[[links]]
  label = "Speakerdeck"
  link = "https://speakerdeck.com/neothethird/linux-in-der-hosentasche"
[[links]]
  label = "Schedule"
  link = "https://talks.oio.social/36c3-oio/talk/E77J8R/"
+++
