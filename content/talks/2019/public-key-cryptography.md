+++
authors = ["Jan Sprinz"]
date = "2019-10-31"
title = "Public Key Cryptography"
subtitle = "An Introduction"
conference = "Bachelorseminar “Ausgewählte Kapitel der Informatik” der LMU"
city = "München"
description = "A quick intro to public key cryptography at the example of the RSA algorithm. Presented at an undergraduate computer science seminar at LMU in the winter term of 2019."
series = ["Bachelorseminar"]
[[links]]
  label = "beamer"
  link = "https://github.com/NeoTheThird/talks/blob/master/crypto.zip"
[[links]]
  label = "Speakerdeck"
  link = "https://speakerdeck.com/neothethird/public-key-cryptography"
+++
