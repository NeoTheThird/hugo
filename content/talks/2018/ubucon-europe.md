+++
authors = ["Jan Sprinz"]
date = "2018-04-28"
title = "One year after the world ended"
subtitle = "Ubuntu Touch today"
conference = "Ubucon Europe"
city = "Xixon"
description = "A closer look at what the UBports Community has been up to in the first year as the maintainer of the Ubuntu Touch operating system."
youtube = "2VSmo9CihBY"
featured_image = "/images/xixon.jpeg"
featured = true
[[links]]
  label = "ODP"
  link = "https://github.com/NeoTheThird/talks/blob/master/ubucon-2018-english.odp"
[[links]]
  label = "Speakerdeck"
  link = "https://speakerdeck.com/neothethird/ubuntu-touch-is-alive-meet-the-ubports-community"
+++
