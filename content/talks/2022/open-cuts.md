+++
authors = ["Johannah Sprinz"]
date = "2022-02-10"
title = "Crowdsourced User-Testing"
subtitle = "Bachelor Thesis Presentation \"Leveraging Human Computation for Quality Assurance in Open Source Communities\""
conference = "Oberseminar Knowledge Representation and Reasoning, LMU Munich"
city = "Munich"
featured_image = "https://files.speakerdeck.com/presentations/af20a176e6244e72877881eda22ab59f/preview_slide_0.jpg?20400660"
featured = true
series = ["OPEN-CUTS"]
mp4 = "https://tib.flowcenter.de/mfc/medialink/3/de0a98cb5b6c6beb6c64b057dba0083b1cfcab5b4b36dad7a4f5091fd84f57c056/Sprinz_2022_Crowdsourced-User-Testing_Video.mp4"
pdf = "https://epub.ub.uni-muenchen.de/91086/2/Sprinz_2022_Crowdsourced-User-Testing_Slides.pdf"
[identifiers]
  doi = "10.5446/56345"
  urn_nbn = "urn:nbn:de:bvb:19-epub-91086-5"
[[links]]
  label = "Thesis"
  link = "/publications/2022/open-cuts"
[[links]]
  label = "TIB AV"
  link = "https://av.tib.eu/media/56345"
[[links]]
  label = "Speakerdeck"
  link = "https://speakerdeck.com/neothethird/crowdsourced-user-testing"
[[links]]
  label = "Schedule"
  link = "https://uni2work.ifi.lmu.de/course/W21/IfI/OS-KR"
[[links]]
  label = "Open Access LMU"
  link = "https://epub.ub.uni-muenchen.de/91086/"
[[links]]
  label = "ResearchGate"
  link = "https://www.researchgate.net/publication/358587740_Crowdsourced_User-Testing"
+++

The [presented thesis](/publications/2022/open-cuts) investigates facilitating software quality assurance in open source communities through a human computation platform. Inexperienced community members can contribute formalized user testing data, which is then aggregated and presented to the developers. The implemented prototype, named [open crowdsourced user-testing suite (OPEN-CUTS)](https://www.open-cuts.org), was evaluated in a usability study in the [UBports Community](https://ubports.com). The viability of this approach has been demonstrated, and further goals for research and development are proposed.
