+++
date = "2022-01-29"
authors = ["Johannah Sprinz"]
title = "Leveraging Human Computation for Quality Assurance in Open Source Communities"
featured_image = "/images/open-cuts.png"
publisher = "LMU Munich, Bachelor Thesis"
city = "Munich"
#original_link = "https://www.en.pms.ifi.lmu.de/publications/index.php#BA_Johannah.Sprinz"
pdf = "http://www.pms.ifi.lmu.de/publikationen/projektarbeiten/Johannah.Sprinz/BA_Johannah.Sprinz.pdf"
mp4 = "https://tib.flowcenter.de/mfc/medialink/3/de0a98cb5b6c6beb6c64b057dba0083b1cfcab5b4b36dad7a4f5091fd84f57c056/Sprinz_2022_Crowdsourced-User-Testing_Video.mp4"
series = ["OPEN-CUTS"]
featured = true
[identifiers]
  doi = "10.5282/ubm/epub.91046"
  urn_nbn = "urn:nbn:de:bvb:19-epub-91046-3"
  wikidata = "Q110802764"
  fatcat_work = "uhc2zbesmzhkxigivv46zamrs4"
  fatcat_release = "t5pskdrbh5cd5p56ecqta3sbsq"
[[links]]
  label = "Presentation"
  link = "/talks/2022/open-cuts"
[[links]]
  label = "Open Access LMU"
  link = "https://epub.ub.uni-muenchen.de/91046/"
[[links]]
  label = "ResearchGate"
  link = "https://www.researchgate.net/publication/358263480_Leveraging_Human_Computation_for_Quality_Assurance_in_Open_Source_Communities"
+++

Software developed under the open source development model (OSSD) has risen to significant importance over the recent decades. With more and more critical components being developed under the OSSD, the need for extensive quality assurance (QA) increases. This thesis investigates any potential for conducting formalized user testing through inexperienced volunteer community members under the OSSD. A human computation platform to aggregate such test results was designed and named [open crowdsourced user-testing suite (OPEN-CUTS)](https://www.open-cuts.org). A usability study of a prototype of OPEN-CUTS confirms the viability of this approach and points to potential future research questions.
