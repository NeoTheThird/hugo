+++
date = "2022-05-19"
authors = ["Johannah Sprinz"]
title = "Warum Hochschulen die Identität von trans Studierenden respektieren sollten"
#featured_image = ""
publisher = "ResearchGate"
preprint = true
#city = ""
#original_link = ""
pdf = "https://www.cip.ifi.lmu.de/~sprinz/sprinz_2022_critical-essay-trans-personen-hochschulen.pdf"
#mp4 = ""
#series = []
featured = true
[identifiers]
  doi = "10.13140/rg.2.2.22490.41923"
  fatcat_work = "gel3tfmpunfefm572ufp2fh2gq"
  fatcat_release = "nf5ervm7v5azpd7tfdkoyzande"
+++

Die aktuelle Gesetzeslage in Deutschland sieht für amtliche Namens- und Personenstandsänderungen von trans Personen ein langwieriges, erniedrigendes und kostspieliges Gerichts- und Begutachtungsverfahren vor. Viele Hochschulen und Universitäten führen Studierende unter amtlichem Namen und Geschlechtseintrag, wodurch trans Studierende häufig erheblicher Diskriminierung ausgesetzt sind. Der vorliegende Critical Essay führt basierend auf einer Darstellung der aktuellen Situation Gründe an, warum Hochschulen trans Studierenden die Verwendung des selbst gewählten Vornamens und der zur geschlechtlichen Identität passenden Anrede bereits vor amtlicher Namens- und Personenstandsänderung ermöglichen sollten und verweist auf bestehende Programme und Handlungsempfehlungen.
