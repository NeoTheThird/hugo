+++
title = "OPEN-CUTS"
description = "open crowdsourced user-testing suite"
featured_image = "/images/open-cuts.png"
featured = true
gitlab = "open-cuts/open-cuts"
website = "https://www.open-cuts.org"
[[links]]
  label = "Demo"
  link = "https://ubports.open-cuts.org"
series = ["OPEN-CUTS"]
+++

OPEN-CUTS is a web-based software suite that allows open source community members to easily contribute user test results. The software aggregates and analyzes those reports to provide useful information for developers.

You will find more information [here](https://www.open-cuts.org) shortly. For now, you can head over to [ubports.open-cuts.org](https://ubports.open-cuts.org) for a demo installation used by the [UBports Community](https://ubports.com). OPEN-CUTS is still very much in development; get involved on [GitLab](https://gitlab.com/open-cuts/open-cuts)!

It has been developed evaluated as part of my [Bachelor Thesis](/publications/2022/open-cuts/).

![OPEN-CUTS in action](https://www.open-cuts.org/preview.gif)
