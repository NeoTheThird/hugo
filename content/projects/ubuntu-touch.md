+++
title = "Ubuntu Touch"
description = "beautiful and privacy-focused open-source mobile operating system"
featured_image = "/images/ubuntu-touch-convergence.png"
featured = true
website = "https://ubuntu-touch.io"
youtube = "_v3CdCTJQms"
[[links]]
  label = "Supported Devices"
  link = "https://ubuntu-touch.io"
[[links]]
  label = "Wikipedia"
  link = "https://en.wikipedia.org/wiki/Ubuntu_Touch"
+++

Ubuntu Touch is a beautiful mobile operating system that respects it's users privacy. The project was originally developed by Canonical Ltd. and is now maintained by the UBports Community.
