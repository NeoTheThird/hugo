+++
date = "2022-02-01"
title = "Some new publications"
description = ""
math = false
tags = ["research"]
categories = []
series = []
+++

It has gotten awfully quiet on here recently - my apologies! Let me just use this opportunity to shine some light on some recent new additions to this website. Most importantly, my [bachelor thesis](/publications/2022/open-cuts/) has finally been published!

You may have already heard me talk about this on the UBports Q&A a while back. Basically, the idea is to do software testing in open source communities by having inexperienced members report manual tests to an easy-to-use online tool. This led to the creation of the open crowdsourced user testing suite [OPEN-CUTS](https://www.open-cuts.org), which we [tested in the UBports community](https://ubports.open-cuts.org).

You can now find my thesis [among the LMU Munich Teaching and Research Unit Programming and Modelling Languages publications website](http://www.pms.ifi.lmu.de/publikationen/projektarbeiten/Johannah.Sprinz/BA_Johannah.Sprinz.pdf). I will be presenting my findings on February 10. 2022 at 10:00 CET in the [Knowledge Representation and Reasoning seminar at LMU Munich](https://uni2work.ifi.lmu.de/course/W21/IfI/OS-KR). You can chime in via zoom, thanks pandemic. If you'd like to connect, hit me up on [ResearchGate](https://www.researchgate.net/publication/358263480_Leveraging_Human_Computation_for_Quality_Assurance_in_Open_Source_Communities) or [Academia.edu](https://www.academia.edu/70206657/Leveraging_Human_Computation_for_Quality_Assurance_in_Open_Source_Communities).

FYI, I also added a [seminar paper on first-order logic unification](/publications/2021/unification/) from a while back. If you're into weird ancient logic formalisms and niche algorithms, that might be interesting for you. No original research, just revisiting some stuff from the early days of computing.
