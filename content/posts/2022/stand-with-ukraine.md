+++
date = "2022-02-24"
title = "#StandWithUkraine"
description = ""
math = false
tags = ["politics"]
categories = []
series = []
featured_image = "https://lh5.googleusercontent.com/RyTldfXU5niraKcZihWxa_L5e-L5qWo-DblVNspgLwnTg_HX5Lk-lppIfLY060c0vgObBeqAqdZuR9G8Die8qCnI3mzylEuvRV2MP2ZhFto25Ykj4b_ma8bZwWbjjdiMHQ=w1280"
+++

Europe needs to stand united with Ukraine against Russian Aggression! Join a [protest in your city](https://www.stopputin.net/) and make your voice heard. The time to prevent world war three is now.
