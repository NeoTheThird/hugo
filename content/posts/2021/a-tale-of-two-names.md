+++
date = "2021-10-18"
title = "A tale of two Names"
description = "meet Johannah \"Hannah\" Sprinz (that's me)"
math = false
tags = ["personal"]
categories = []
series = []
featured_image = "/images/avatar.jpeg"
+++

The time has come to start using my proper name! So, hey everyone, I am Johannah (or Hannah for short). Pronouns are [she/they](http://my.pronoun.is/she?or=they); in German [sie](https://de.wiktionary.org/wiki/sie). That's... pretty much it. Thanks!

(In case you missed it, I [came out as nonbinary](https://twitter.com/NeoTheThird/status/1344644001685786627) last year and [haven't shut up about it since](/posts/2021/new-design). There are some [answers to frequently asked questions](/about/#faq) available.)
