+++
date = "2021-09-15"
title = "New design"
description = "*smiles in nonbinary*"
math = false
tags = ["hugo", "personal"]
categories = []
series = []
+++

Hey all! Finally got around to fixing the GitLab Pages deployment on this site. I also used this opportunity to tweak the css a little. In case you missed it, I [came out as nonbinary](https://twitter.com/NeoTheThird/status/1344644001685786627) last year, so the new color scheme is inspired by redditor [u/loonycatty](https://www.reddit.com/u/loonycatty)'s [non-neon version](https://www.reddit.com/r/ennnnnnnnnnnnbbbbbby/comments/ibfxxq/the_neon_colors_in_the_flag_kind_of_bother_my/) of the [nonbinary flag](https://en.wikipedia.org/wiki/Non-binary_gender#Symbols_and_observances). Looks cozy, no? Not sure if this was the wisest move in terms of readability - but hey, you win some, you loose some.

No idea how things will continue here, blog-wise. I guess you'll have to stick around to find out! :)
